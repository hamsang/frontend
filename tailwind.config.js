module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          "Vazir",
          "ui-sans-serif",
          "system-ui",
          "Roboto",
          "Arial",
          "Noto Sans",
        ],
      },
    },
  },
  plugins: [],
};
