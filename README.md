# Hamsang Web Interface

A web UI for Hamsang project.

![Screenshot](./screenshot.gif)

## Run Locally

Clone the repo and run `npm install` to install all nodejs dependencies.

Make sure you set up API URL either as environment variable make `.env.local` and set the URL for `REACT_APP_API_URL`.

Run `npm run start` for to run the project on a temporary live server.

## Build

Run `npm run build` in order to build the project. Copy `build` directory to your desired destination to run.

## License

This project is a Free/Libre and Open Source Software released under AGPL license.
