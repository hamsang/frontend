import { useEffect, useState } from "react";
import "./index.css";
import Results from "./componetns/Results";
import axios from "axios";

function App() {
  const [data, setData] = useState([]);
  const [activeDict, setActiveDict] = useState(null);
  const [searchWord, setSearchWord] = useState("سلام");
  const [searchInput, setSearchInput] = useState(searchWord);

  const URL = process.env.REACT_APP_API_URL;

  useEffect(() => {
    if (!searchWord) return;

    axios
      .get(`${URL}word/${searchWord}`)
      .then((res) => {
        setData(res.data.data);
      })
      .catch((err) => console.log(err));
  }, [searchWord, URL]);

  useEffect(() => {
    if (data.length) {
      setActiveDict(data[0].dict_name);
    }
  }, [data]);

  const onSearchClick = (e) => {
    e.preventDefault();
    setSearchWord(searchInput.trim());
  };

  const onDictClick = (title) => {
    setActiveDict(title);
  };

  return (
    <div
      className="
        min-h-screen
        bg-gray-200
        p-4
        flex flex-col
        justify-center
        sm:py-12
        font-sans
      "
      dir="rtl"
    >
      <div className="relative py-3 sm:max-w-4xl w-full sm:mx-auto">
        <div
          className="
            max-h-screen
            bg-white
            p-8
            shadow-lg
            border-2 border-gray-300
            rounded-3xl
            flex flex-col
          "
        >
          <section id="top-bar">
            <form className="flex" onSubmit={onSearchClick}>
              <input
                className="border border-gray-400 rounded-r-lg border-l-0 py-2 px-4 flex-grow"
                type="text"
                name="search"
                id="search"
                placeholder="جستجوی واژه"
                onChange={(e) => setSearchInput(e.target.value)}
                value={searchInput}
              />
              <input
                type="submit"
                value="جستجو"
                className="py-2 px-4 text-white font-bold text-sm rounded-l-lg bg-blue-500 cursor-pointer hover:bg-blue-600 active:bg-blue-700"
              />
            </form>
          </section>
          <Results
            data={data}
            activeDict={activeDict}
            onDictClick={onDictClick}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
