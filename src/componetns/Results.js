// import { Result } from "postcss";
import React from "react";
import DictItem from "./DictItem";
import Result from "./Result";

const Results = ({ data, activeDict, onDictClick }) => {
  return (
    <section id="results" className="mt-4 h-96 flex flex-col">
      <ul id="tabs" className="flex gap-2">
        {data.length ? (
          data.map((d) => (
            <DictItem
              title={d["dict_name"]}
              count={d.result.length}
              activeDict={activeDict}
              onDictClick={onDictClick}
              key={d["dict_name"]}
            />
          ))
        ) : (
          <DictItem title="-" count="0" />
        )}
      </ul>

      <Result
        data={
          data.length && activeDict
            ? data[0].result
            : [
                {
                  word: "چیزی نمایش نیست",
                  definition: "واژه‌ای (دیگر) را جستجو کنید.",
                },
              ]
        }
      />
    </section>
  );
};

Result.defaultProps = {
  activeDict: "",
};

export default Results;
