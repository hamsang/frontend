import React from "react";

const DictItem = ({ title, count, activeDict, onDictClick }) => {
  const bgColor =
    activeDict === title ? " bg-blue-400" : " bg-blue-100 hover:bg-blue-200";
  const className =
    "px-4 py-2 text-black font-bold flex gap-1 cursor-pointer rounded-t-md transition-colors duration-200";
  return (
    <li
      className={className + " " + bgColor}
      onClick={() => onDictClick(title)}
    >
      {title}
      {count > 0 ? (
        <span className="bg-white text-gray-800 px-2 py-1 rounded-full text-xs">
          {count}
        </span>
      ) : (
        ""
      )}
    </li>
  );
};

DictItem.defaultProps = {
  title: "",
  count: 0,
};
export default DictItem;
