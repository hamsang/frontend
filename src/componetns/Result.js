import React from "react";

function Result({ data }) {
  return (
    <div
      id="result"
      className="
                pt-2
                grid
                sm:grid-cols-2
                grid-flow-row
                gap-4
                font-thin
                text-gray-700
                overflow-auto
                border-t-4 border-blue-400
              "
    >
      {data.map((d) => (
        <div className="result-item w-full p-1" key={d.word}>
          <h5 className="text-red-500 font-bold">{d.word}</h5>
          <p
            dangerouslySetInnerHTML={{
              __html: d.definition.replace(/\n/g, "<br />"),
            }}
          />
        </div>
      ))}
    </div>
  );
}

export default Result;

/*
<div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه ۱</h5>
        <p>محتوای مربوط به این واژه خاص</p>
        <p>محتوای مربوط به این واژه خاص</p>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه ۲</h5>
        <p>محتوای مربوط به این واژه خاص محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه ۳</h5>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه ۴</h5>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>
          محتوای مربوط به این واژه خاص محتوای مربوط به این واژه خاص محتوای مربوط
          به این واژه خاص
        </p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>
          محتوای مربوط به این واژه خاص محتوای مربوط به این واژه خاص محتوای مربوط
          به این واژه خاص
        </p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>
          محتوای مربوط به این واژه خاص محتوای مربوط به این واژه خاص محتوای مربوط
          به این واژه خاص
        </p>
      </div>
      <div className="result-item w-full">
        <h5 className="text-red-500 font-bold">واژه</h5>
        <p>محتوای مربوط به این واژه خاص</p>
      </div>
*/
